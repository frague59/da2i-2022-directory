"""
Admin for Directory.
"""
import logging

from django.contrib import admin
from django.utils.translation import gettext as _
from django.utils.safestring import SafeString, mark_safe

from directory import models

logger = logging.getLogger(__name__)

admin.site.site_header = _("Directory management")
admin.site.site_title = _("Directory management")


@admin.register(models.Civility)
class CivilityAdmin(admin.ModelAdmin):
    """Admin for Civility."""

    list_display = ("name", "short_name")
    search_fields = ("name", "short_name")


@admin.register(models.Student)
class StudentAdmin(admin.ModelAdmin):
    """Admin for Student."""

    list_display = ("__str__", "civility", "birth_date", "get_photo", "class_level", "get_class_options")
    search_fields = ("name", "civility__name", "birth_date")
    fieldsets = (
        (
            None,
            {
                "fields": [
                    "civility",
                    ("last_name", "first_name"),
                    "birth_date",
                    "photo",
                ],
            },
        ),
        (
            _("Contact"),
            {"fields": ["email", "phone"]},
        ),
        (
            _("Class and options"),
            {"fields": ["class_level", "class_options"]},
        ),
    )

    @admin.display(description="Class options")
    def get_class_options(self, obj) -> SafeString:
        """Return class options."""
        output = ["<ul>"]
        output.extend([f"<li>{option.name}</li>" for option in obj.class_options.all()])
        output.append("</ul>")
        return mark_safe("".join(output))

    @admin.display(description="Image")
    def get_photo(self, obj: models.Student) -> SafeString:
        """Return image."""
        if obj.photo:
            return mark_safe(f'<img src="{obj.photo.url}" width="120" title="{obj.get_full_name()}"/>')


@admin.register(models.ClassOption)
class ClassOptionAdmin(admin.ModelAdmin):
    """Admin for ClassOption."""

    list_display = ("__str__", "class_level")
    search_fields = ("name", "class_level")


@admin.register(models.ClassLevel)
class ClassLevelAdmin(admin.ModelAdmin):
    """Admin for ClassLevel."""

    list_display = ("__str__", "level")
    search_fields = ("name",)
